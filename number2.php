<!-- There are two deals of an item to buy. The quantities and prices of the item are given below. 
Write a program in PHP to find the best deal to purchase the item. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    
<div class="container d-flex justify-content-center">
    <div class="card mt-5 w-50 border-success">
        <div class="card-header text-center h1 text-white bg-success">
             FIND THE BEST DEAL
        </div>
        <div class="card-body">  
            <?php  
                $quantity1 = 70;
                $quantity2 = 100;
                $price1 = 35;
                $price2 = 30;

                $deal1 = $price1 / $quantity1;
                $deal2 = $price2 / $quantity2;

                $bestDeal;
                echo ("Price over quantity to find the best deal");
                echo"<hr>";

                echo "FIRST DEAL:  <br>";

                echo ucfirst("quantity 1  = $quantity1"."<br>");
                echo ucfirst("price 1     = $price1"."<br>");
                echo "<br>";
                echo "SECOND DEAL: <br>";

                echo ucfirst("quantity 2  = $quantity2"."<br>");
                echo ucfirst("price 2     = $price2"."<br>");
                echo"<hr>";

                echo "BEST DEAL: <br>";
                if($deal1 > $deal2){
                    echo strtoupper("The second deal is best!");
                }else{
                    echo strtoupper("The first deal is best!");
                }
            ?>
        </div>
    </div>
</div>


</body>
</html>
