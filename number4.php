<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <title>Document</title>
</head>
<body>

<div class="container d-flex justify-content-center">
    <div class="card mt-5 w-50 border-success  text-center">
        <div class="card-header h1 text-white bg-success">
             CHECK IF NUMBER IS ARMSTRONG
        </div>
        
          <div class="card-body mt-5">

            <form method="post">
              <input type="text" name="number" placeholder="Enter your desired digit" class="p-2 w-75">
              <br>
              <input type="submit" name="submit" class = "btn btn-primary mt-4 mb-2" value="Check Number">
            </form>

            <?php  

              if (isset($_POST['submit'])) {
                  
                  if (empty($_POST["number"])) {
                      echo"<hr>";
                      echo "<h3>Answer goes here<h3>";    
                  }else{ 

                      $num  = $_POST['number'];
                      $sum = 0;  
                      $temp = $num;  
                      while($temp != 0){  
                        $digit = $temp % 10;  
                        $sum += $digit ** 3;
                        $temp = $temp/10;  
                      }  
                      echo"<hr>";
                      if($num==$sum)  {  
                          echo ("<h2>$num<h2> <h3>is an Armstrong number<h3>");  
                      }else{  
                          echo ("<h2>$num<h2> <h3>is not an Armstrong number<h3>");  
                      }   
                  }
                
              }
            ?>  

        </div>
    </div>
</div>

</body>
</html>