<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<style>

tr, th, td{
    padding: 10px;
    border: 2px solid black;
}

</style>

<body>
    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-75 border-white">
            <div class="card-header text-center h1 text-white bg-success">
            DIVISION TABLE WITH PHP
            </div>
            <div class="card-body">  
                <table class="table w-100 text-center">
                    <?php
                        $start_num = 1;
                        $end_num = 10;
                    
                        print("<th> </th>");

                        for ($numHeader = $start_num; $numHeader <= $end_num; $numHeader++){
                            print("<th> $numHeader </th>");
                        }
                      

                        for ($numSide = $start_num; $numSide <= $end_num; $numSide++){
                            print("<tr> <th> $numSide </th>");
                            for ($numHeader = $start_num; $numHeader <= $end_num; $numHeader++){
                                $answer = $numHeader / $numSide;
                                printf("<td> %.2f </td>",
                                $answer); 
                            }
                            print("</tr>\n");
                        }
                    ?> 
                </table>
            </div>
        </div>
    </div>


</body>
</html>

  

