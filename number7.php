<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>

<style>
*{
    font-weight: bold;
}
</style>
<body>
    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-50 border-success  text-center">
            <div class="card-header h1 text-white bg-success">
                CONVERT DIGIT TO WORD
            </div>
            
            <div class="card-body mt-2">
                <form method="post">
                <input type="text" name="number" placeholder="Enter a number" class="p-2 w-75">
                <br>
                <input type="submit" name="submit" class = "btn btn-primary mt-4 mb-2" value="Convert Digit">
                </form>

            

            <?php  
                if (isset($_POST['submit'])) {
                                    
                    if (empty($_POST["number"])) {
                        echo"<hr>";
                        echo "Answer goes here"; 

                    }else{ 

                        $numArr = array(
                            0 =>"ZERO",
                            1 => "ONE",
                            2 => "TWO",
                            3 => "THREE",
                            4 => "FOUR",
                            5 => "FIVE",
                            6 => "SIX",
                            7 => "SEVEN",
                            8 => "EIGHT",
                            9 => "NINE"
                        );

                        $string= $_POST["number"];
                        $length = strlen($string);
                        $thisWordCodeVerdeeld = array();

                        echo "<hr>";
                        
                        for ($ndx = 0; $ndx < $length; $ndx++) {
                            $num[$ndx] = $string[$ndx];
                            foreach ($numArr as $key => $value) {
                                $intNum = $num[$ndx];
                                if($intNum == $key ){
                                    echo  $value . " " ;
                                }
                            }
                        } 

                    }
                        
                }
            ?>  
            </div>
        </div>
    </div>
</body>
</html>