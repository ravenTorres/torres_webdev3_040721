<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Number 1</title>
</head>

<style>
/* *{
    font-size: 20px;
} */
</style>
<body>

<div class="container d-flex justify-content-center">
    <div class="card mt-5 w-75 border-success text-justify">
        <div class="card-header text-center h1 text-white bg-success">
             LOOP OVER A JSON DATA
        </div>
        <div class="row">
            <div class="card-body col-lg-6 col-md-12 col-sm-12 pl-5" name="left">
            <div class="card-title">
                <h3>TRY 1 (unorganized)</h3>
                <br>
            </div>
            <?php 

                $json ='[
                        {
                            "name" : "John Garg",
                            "age"  : "15",
                            "school" : "Ahlcon Public school"
                        },
                        {
                            "name" : "Smith Soy",
                            "age"  : "16",
                            "school" : "St. Marie school"
                        },
                        {
                            "name" : "Charle Rena",
                            "age"  : "16",
                            "school" : "St. Columba school"
                        }
                    ]';
                        
                $json = json_decode($json,true);

                    foreach($json as $key => $values){
                        foreach($values as $data => $json_data){
                            echo  ("$data is $json_data" ."<br><br>");
                        }
                    }

            ?>
            </div>
            <div class="card-body col-lg-6 col-md-12 col-sm-12 border-left pl-5" name = "right">
            <div class="card-title">
                <h3>TRY 2 (Format)</h3>
                <br>
            </div>
            <?php 
                foreach($json as $values){
                    echo "Name: " .$values["name"]."<br>";
                    echo "Age: " .$values["age"]."<br>";
                    echo "School: " .$values["school"]."<br>";
                    echo "<hr>";
                }
            ?>
            </div>
        </div>
    </div>
</div>
<?php

?>
</body>
</html>

