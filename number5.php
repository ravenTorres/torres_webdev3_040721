<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-50 border-success">
            <div class="card-header text-center h1 text-white bg-success">
                DELETE RECURRING INTEGER IN AN ARRAY
            </div>
            <div class="card-body m-2">  
                
                <?php
                    $intArr = array(1, 1, 2, 1, 3, 5, 4, 6, 4, 7, 8);
                    $uniqueArr = array_unique($intArr);

                    echo strtoupper("original array: <br>");
                    print_r($intArr);


                    echo "<br><br>";

                    echo strtoupper("unique values in an array: <br> ");
                    print_r($uniqueArr);

                    echo "<br><hr>";

                    echo "Formatted Original Array :   ";
                    foreach($intArr as $value1){
                        echo  $value1 .",  ";  
                    }
                    
                    echo "<br><br>";

                    echo "Formatted Unique Values :   ";
                    foreach($uniqueArr as $value2){
                        echo  $value2 .",  ";  
                    }
                ?>
            </div>
        </div>
    </div>

</body>
</html>
